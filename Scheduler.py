from Order import Order
from DatabaseInterface import *
from Route import *

class Scheduler:
    def __init__(self):
        self.pendingOrders = []
        self.busListing = []
        self.workingRoutes = Route()
        
    def timestep(self):
        if len(self.pendingOrders) == 0: return
        
        nextOrder = self.pendingOrders[0]
        self.pendingOrders = self.pendingOrders[1:]
        
        self.busListing = getBusListing(nextOrder.timing)
        self.workingRoutes = fetchRouteInfo(self.busListing[0].id, nextOrder.timing - nextOrder.timing % 10)
        
        if self.workingRoutes.insertWaypoint(nextOrder.start, nextOrder.timing - 1) and \
                self.workingRoutes.insertWaypoint(nextOrder.end, nextOrder.timing):
            writeRouteInfo(self.workingRoutes, self.busListing[0].id, nextOrder.timing - nextOrder.timing % 10)
            
        else: print("Failed to route: ", nextOrder)
            
        
    
    def dontUseThisYetItsNotReady(self):
        if len(self.pendingOrders) == 0: return
        
        nextOrder = self.pendingOrders[0]
        self.pendingOrders = self.pendingOrders[1:]
        
        self.busListing = getBusListing(nextOrder.timing)
        self.workingRoutes = []
        for i in range(len(self.busListing)):
            self.workingRoutes.append(fetchRouteInfo(self.busListing[i], nextOrder.timing))
            
        originalDistances = []
        for i in range(len(self.busListing)):
            originalDistances.append(self.workingRoutes[i].getLength())
            
        for i in range(len(self.busListing)):
            if self.workingRoutes[i].insertWaypoint(nextOrder.end, nextOrder.timing):
                pass
                
            