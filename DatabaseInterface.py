from Bus import Bus
from Route import Route

RouteStorage = {}

def getBusListing(day):
    # print("We just gave the default list for now")
    x = [Bus(1, 11, True),
         Bus(2, 10, False),
         Bus(3, 20, False),
         Bus(4, 20, False),
         Bus(5, 20, False),
         Bus(6, 30, False)
         ]
    return x

def writeRouteInfo(routeToWrite, busID, day):
    RouteStorage[day, busID] = routeToWrite
    print(routeToWrite)
    
def fetchRouteInfo(busID, day):
    if RouteStorage.keys().__contains__((day, busID)) :return RouteStorage[day, busID]
    else: return Route()