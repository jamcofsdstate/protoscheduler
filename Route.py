from math import sqrt

class Route:
    def __init__(self):
        self.waypoints = []
        
    def insertWaypoint(self, location, time):
        if len(self.waypoints) == 0:
            self.waypoints = [(time, location)]
            return True
        elif self.waypoints[0][0] > time:
            self.waypoints = [(time, location)] + self.waypoints
            return True
        elif self.waypoints[-1][0] < time:
            self.waypoints.append((time, location))
            return True
        else:
            for i in range(len(self.waypoints)-1):
                if self.waypoints[i][0] < time and self.waypoints[i+1][0] > time:
                    self.waypoints = self.waypoints[:i] + [(time, location)] + self.waypoints[i+1:]
                    return True
                
        return False
    
    def getLength(self):
        distance = 0
        for i in range(len(self.waypoints)-1):
            distance += dist(self.waypoints[i][1], self.waypoints[i+1][1])
            
        return distance
        
    def __repr__(self):
        representation = ""
        for w in (self.waypoints): representation += ' ' + str(w[1])
        return representation
    

def dist(s, f):
    return sqrt((s[0] - f[0]) ** 2 + (s[1] - f[1]) ** 2)