import pygame
from pygame.locals import *
from random import randint
from Scheduler import Scheduler
from Order import Order

pygame.init()


def main():
    """
    :return: None
    """
    
    #initialize display window with a black background and a 400 X 400 space
    window = pygame.display.set_mode((400, 400))
    background = pygame.Surface(window.get_size()).convert()
    background.fill((0, 0, 0))
    
    #list used to draw the path for the bus
    trail = []
    
    scheduler = Scheduler()
    
    # main loop
    done = False
    mouseDrag = False
    startLocation = (0,0)
    endLocation = (0,0)
    selectedTime = 20
    while not done:
        for event in pygame.event.get():
            #escape exits
            if event.type == KEYDOWN and event.key == K_ESCAPE: done = True
            elif event.type == KEYDOWN and event.key == K_0: print(trail)
            #window "X" exits as well
            elif event.type == QUIT: done = True
            
            elif (event.type == MOUSEBUTTONDOWN and event.button == 1):
                mouseDrag = True
                startLocation = pygame.mouse.get_pos()
                
            elif (event.type == MOUSEBUTTONUP and event.button == 1):
                mouseDrag = False
                scheduler.pendingOrders = [Order(startLocation, pygame.mouse.get_pos(), selectedTime, False)] + scheduler.pendingOrders
                selectedTime += 2
                
                
            #        or (event.type == MOUSEMOTION and pygame.mouse.get_pressed()[0]):
            #  click_marker.topleft = event.pos
            #  trail = trail[1:] + [event.pos]
            
        #let the scheduler have turn to run
        scheduler.timestep()
        
        trail = []
        for i in range(len(scheduler.workingRoutes.waypoints)):
            trail.append(scheduler.workingRoutes.waypoints[i][1])
        
        #This constitutes the drawing update for the window
        window.blit(background, (0, 0))
        #for i in range(complexity):
        if len(trail) > 1 : pygame.draw.lines(window, (255,0,0), False, trail, 2)
        if mouseDrag: pygame.draw.line(window, (0,0,255), startLocation, pygame.mouse.get_pos())
        #for i in range(complexity): pygame.draw.circle(window, customers[i].color, customers[i].start, 5)
        #for i in range(complexity): pygame.draw.circle(window, customers[i].color, customers[i].end, 3)
        #pygame.draw.lines(window, (255, 0, 0), False, trail, 3)
        pygame.display.update()
        

main()
